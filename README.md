# Inverted Pendulum Simulation and control

A 2d simulation of an inverted pendulum using pymunk and pygame, for research on what control method can achieve balance best.

Criterion for a good balance method: 
1. Should minimize deviation from normal
2. Should be resistant to variable 'wind'
3. Should be able to cope with variable friction
4. Should be able to cope with variable slope

Installation Instructions:
```bash
git checkout git@gitlab.com:Ev3ryDay1sL3gDay/inverted-pendulum-simulation-and-control.git
cd inverted-pendulum-simulation-and-control/
pipenv install
pipenv run python sim.py
```
