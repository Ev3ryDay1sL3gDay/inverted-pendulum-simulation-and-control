from pygame.locals import *
from pymunk import Vec2d


def force_callBack_circle(sim, reset):
    return ((0, sim.base_radius), (0, 0))


def force_callBack_rectangle(sim, reset):
    return ((0, 0), (0, 0))


class Controller(object):
    def __init__(self, force_callBack=force_callBack_rectangle):
        """forcecallBack should be a function that accepts Simulation object and reset boolean, and returns a 2d (x, y) relative position and a 2d (x, y) force Vector that will be applied to sim.base_shape.body
        """
        self.force_callBack = force_callBack

        def callBack(paused, events, keys_pressed, sim):
            paused, stop, reset = False, False, False
            for event in events:
                if event.type == KEYDOWN and event.key == K_p:
                    paused = True
                elif event.type == KEYUP and event.key == K_r:
                    reset = True
                elif (event.type == QUIT) or (event.type == KEYDOWN and event.key == K_q):
                    stop = True
            if not paused:
                force_position, force_vector = self.force_callBack(sim, reset)
                sim.base_shape.body.apply_force_at_local_point(
                    100 * (Vec2d(force_vector) +
                           (keys_pressed[K_RIGHT] - keys_pressed[K_LEFT]) *
                           sim.base_shape.body.mass *
                           sim.pendulum_shape.body.mass *
                           Vec2d(1, 0)),
                    force_position)
            return paused, stop, reset
        self.callBack = callBack
