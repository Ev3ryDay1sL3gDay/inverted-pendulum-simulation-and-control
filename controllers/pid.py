from controllers.controller import Controller
from datetime import datetime
import sys
sys.path.append("../")


class PID(object):
    def __init__(self, P, I, D, td=1.0):
        self.P, self.I, self.D = P, I, D
        self.Err = .0
        self.Diff = .0
        self.Count = .0
        self.td = td

    def tick(self, Err):
        self.Diff = (Err - self.Err) / self.td
        self.Err = Err
        self.Count += self.Err * self.td
        return (self.P*self.Err + self.I*self.Count + self.D*self.Diff)

    def reset(self):
        self.Err = .0
        self.Diff = .0
        self.Count = .0


class PIDController(Controller):
    def __init__(self, P, I, D, td=1.0, offset=(0, 0), max_mag_force=100):
        self.PID = PID(P, I, D)
        self.offset = offset
        self.max_mag_force = abs(max_mag_force)

        def force_callBack(sim, reset):
            if reset:
                self.PID.reset()
            mag_force = max_mag_force*sim.base_shape.body.mass * sim.pendulum_shape.body.mass
            corr_force = -self.PID.tick(sim.pendulum_shape.body.angle)
            f = 0
            try:
                f = (corr_force/abs(corr_force)) * \
                    min(abs(mag_force), abs(corr_force))
            except:
                pass
            return offset, (f, 0)
        super(PIDController, self).__init__(force_callBack=force_callBack)
