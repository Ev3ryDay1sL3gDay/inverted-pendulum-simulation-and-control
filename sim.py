# Copyright 2018 Anant Sujatanagarjuna<anant.essen@gmail.com>
# This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.

#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

import _thread
import pygame
from pygame.locals import *
import pymunk
import pymunk.pygame_util
from math import pi, sqrt


def dummy_callBack(paused, events, keys_pressed, sim):
    return False, False, False


def default_callBack(paused, events, keys_pressed, sim):
    paused, stop, reset = False, False, False
    for event in events:
        if event.type == KEYDOWN and event.key == K_p:
            paused = True
        elif event.type == KEYUP and event.key == K_r:
            reset = True
        elif (event.type == QUIT) or (event.type == KEYDOWN and event.key == K_q):
            stop = True
    if not paused:
        sim.base_shape.body.apply_force_at_local_point(
            (keys_pressed[K_RIGHT] - keys_pressed[K_LEFT]) * sim.pendulum_shape.body.mass * sim.base_shape.body.mass * pymunk.Vec2d(100, 0), (0, 0))
    return paused, stop, reset


def default_callBack_circle(paused, events, keys_pressed, sim):
    paused, stop, reset = False, False, False
    for event in events:
        if event.type == KEYDOWN and event.key == K_p:
            paused = True
        elif event.type == KEYUP and event.key == K_r:
            reset = True
        elif (event.type == QUIT) or (event.type == KEYDOWN and event.key == K_q):
            stop = True
    if not paused:
        sim.base_shape.body.apply_force_at_local_point(
            100 * (keys_pressed[K_RIGHT] - keys_pressed[K_LEFT]) * sim.base_shape.body.mass * sim.pendulum_shape.body.mass * pymunk.Vec2d(1, 0), pymunk.Vec2d(0, sim.base_radius) * 0.9)
    return paused, stop, reset


class Simulation(object):
    CIRCLE = 0
    RECTANGLE = 1

    def __init__(self,
                 TYPE=CIRCLE,
                 screen_name="Inverted Pendulum Simulation",
                 screen_size=(800, 600),
                 gravity=(0.0, -100),
                 boundary_width=5.0,
                 base_mass=5.0,
                 base_size=(50, 10),
                 base_radius=20,
                 base_friction=1,
                 base_position=(400, 15),
                 pendulum_mass=1,
                 pendulum_size=(10, 100),
                 tickrate=30,
                 callBack=default_callBack_circle):
        """Initialize Inverted Pendulum
        Parameters are self explanatory.
        callBack should be a function that accepts:
         - pause (boolean value indicating whether the simulation is paused)
         - events (output of pygame.event.get())
         - keys_pressed (output of pygame.key.get_pressed()),
         - sim (Simulation object reference)
         and must return booleans pause, stop, reset : that tells the
         object to pause, kill or reset the simulation respectively"""
        pygame.init()
        self.TYPE = TYPE
        self.pause = False
        self.callBack = callBack
        self.screen_size = screen_size
        self.screen = pygame.display.set_mode(self.screen_size)
        pygame.display.set_caption(screen_name)
        self.clock = pygame.time.Clock()
        self.tickrate = tickrate
        self.space = pymunk.Space()
        self.gravity = gravity
        self.space.gravity = self.gravity
        pymunk.pygame_util.positive_y_is_up = True
        self.draw_options = pymunk.pygame_util.DrawOptions(self.screen)
        self.base_mass, self.base_size, self.base_radius, self.base_position, self.base_friction = base_mass, base_size, base_radius, base_position, base_friction
        self.pendulum_mass, self.pendulum_size = pendulum_mass, pendulum_size
        self.add_World_Bounds(boundary_width)
        self.add_pendullum_setup()
        self.tick()

    def add_World_Bounds(self, boundary_width=5.0):
        """Adds world bounds"""
        bound_body = pymunk.Body(body_type=pymunk.Body.STATIC)
        mid_point = pymunk.Vec2d(self.screen_size)//2
        bound_body.position = mid_point

        l1 = pymunk.Segment(bound_body, -mid_point,
                            mid_point * pymunk.Vec2d(-1, 1), boundary_width)
        l2 = pymunk.Segment(bound_body, -mid_point,
                            mid_point * pymunk.Vec2d(1, -1), boundary_width)
        l3 = pymunk.Segment(bound_body, mid_point, mid_point *
                            pymunk.Vec2d(1, -1), boundary_width)
        l4 = pymunk.Segment(bound_body, mid_point, mid_point *
                            pymunk.Vec2d(-1, 1), boundary_width)
        self.bounds = [l1, l2, l3, l4]
        for bound in self.bounds:
            bound.friction = self.base_friction
        self.space.add(l1, l2, l3, l4)

    def add_pendullum_setup(self):
        """Adds pendulum and its  base with a pivot joint to an existing pymunk physics environment"""
        if self.TYPE is self.RECTANGLE:
            base_body = pymunk.Body(mass=self.base_mass,
                                    moment=pymunk.moment_for_box(
                                        self.base_mass, self.base_size),
                                    body_type=pymunk.Body.DYNAMIC)
            self.base_shape = pymunk.Poly.create_box(base_body,
                                                     self.base_size)
        else:
            base_body = pymunk.Body(mass=self.base_mass,
                                    moment=pymunk.moment_for_circle(
                                        self.base_mass, 0, self.base_radius),
                                    body_type=pymunk.Body.DYNAMIC)
            self.base_shape = pymunk.Circle(base_body,
                                            self.base_radius)
        base_body.position = self.base_position
        self.base_shape.friction = 0.5
        pendulum_body = pymunk.Body(mass=self.pendulum_mass,
                                    moment=pymunk.moment_for_box(self.pendulum_mass,
                                                                 self.pendulum_size),
                                    body_type=pymunk.Body.DYNAMIC)
        pendulum_body.position = self.pendulum_position = pymunk.Vec2d(
            self.base_position) + pymunk.Vec2d((0, [self.base_size[1] // 2, self.base_radius][self.TYPE == self.CIRCLE])) + pymunk.Vec2d((0, self.pendulum_size[1] // 2))
        self.pendulum_shape = pymunk.Poly.create_box(
            pendulum_body,
            self.pendulum_size)
        self.pivot_joint = pymunk.PivotJoint(self.pendulum_shape.body,
                                             self.base_shape.body,
                                             self.base_position + [pymunk.Vec2d(self.base_size) * (0, .5), pymunk.Vec2d((0, self.base_radius)) * (.0, .0)][self.TYPE == self.CIRCLE])
        self.pivot_joint.error_bias = pow(1.0 - 0.1, 60.0) * 10
        self.pivot_joint.collide_bodies = False
        self.space.add(self.base_shape.body, self.base_shape)
        self.space.add(self.pendulum_shape.body, self.pendulum_shape)
        self.space.add(self.pivot_joint)

    def tick(self):
        """Goes ahead one timestep"""
        if not self.pause:
            self.screen.fill((255, 255, 255))
            self.space.debug_draw(self.draw_options)
            self.space.step(1/self.tickrate)
            pygame.display.flip()
            self.clock.tick(self.tickrate)
        events = pygame.event.get()
        keys_pressed = pygame.key.get_pressed()
        return events, keys_pressed, self.base_shape, self.pendulum_shape

    def run(self):
        """Runs an infinite loop calling tick(), and runs callBack(self.pause, events, keys_pressed, base_shape, pendulum_shape)"""
        pause, stop = False, False
        while(True):
            events, keys_pressed, base_shape, pendulum_shape = self.tick()
            pause, stop, reset = self.callBack(self.pause,
                                               events,
                                               keys_pressed,
                                               self)
            if pause is True:
                self.pause = not self.pause
            elif stop is True:
                self.pause = True
                pygame.quit()
                break
            elif reset is True:
                self.reset()

    def run_async(self):
        """Runs the simulation asynchronously"""
        try:
            _thread.start_new_thread(self.run, ())
        except Exception as E:
            print(E)

    def reset(self):
        """Resets the simulation"""
        self.pause = True
        self.space.remove(
            self.base_shape, self.pendulum_shape, self.pivot_joint)
        self.base_shape = None
        self.pendulum_shape = None
        self.pivot_joint = None
        self.add_pendullum_setup()
        self.pause = False


if __name__ == '__main__':
    sim = Simulation()
    sim.run()
